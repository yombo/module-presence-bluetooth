#!/usr/bin/env bash

eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"

PYENV_VERSION=`pyenv version | cut -d" " -f 1`
TEMP_MODULE_PATH=`pwd`
echo "Update system and install dependencies"
sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get install -y git build-essential make python3-dev libudev-dev python3-setuptools libbluetooth3 libbluetooth-dev libboost-thread-dev libglib2.0 python-dev
pip3 install pybluez
