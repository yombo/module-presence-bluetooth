#This file was created by Yombo for use with Yombo Gateway automation
#software. Details can be found at https://yombo.net
"""
Bluetooth Presence
==========================

Requires the 'Presence' module to work.

This module can use various bluetooth receivers to track the location
of a bluetooth device.

This module is running has ONLY 1 bluetooth interface, such as one
built into the motherboard, Raspberry Pi 3/Zero w, or a USB interface. To
track devices across multiple locations, module gateway's will need to be
setup and configured as 'slaves' to a master gateway. We recommend either
a Raspberry PI 3 ($35) or Raspberry Pi Zero ($10).

Learn about at: https://yombo.net/
Get started today: https://yombo.net/Docs/Gateway/Quick_start

License
=======

See LICENSE.md for full license and attribution information.

The Yombo team and other contributors hopes that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
more details.

.. moduleauthor:: Mitch Schwenk <mitch-gw@yombo.net>
:copyright: 2017 Yombo
:license: Apache 2.0
"""

# Import python libraries
from collections import OrderedDict
import os
try:
    import bluetooth
    from .rssi import BluetoothRSSI
    HAS_BLUETOOTH = True
except Exception as e:
    HAS_BLUETOOTH = False

try:  # Prefer simplejson if installed, otherwise json will work swell.
    import simplejson as json
except ImportError:
    import json
from time import time

from twisted.internet.defer import inlineCallbacks, Deferred
from twisted.internet.task import LoopingCall
from twisted.internet import reactor, threads

from yombo.core.exceptions import YomboWarning
from yombo.core.log import get_logger
from yombo.core.module import YomboModule
from yombo.lib.webinterface.auth import require_auth
from yombo.utils.fuzzysearch import FuzzySearch
from yombo.utils import global_invoke_all


logger = get_logger("modules.presence")


class BluetoothPresence(YomboModule):
    """
    Uses a bluetooth receiver to detect presence of a device.
    """
    BT_MAJOR_CLASSES = ("Miscellaneous",
                        "Computer",
                        "Phone",
                        "LAN/Network Access point",
                        "Audio/Video",
                        "Peripheral",
                        "Imaging")

    BT_SERVICE_CLASSES = ((16, "positioning"),
                          (17, "networking"),
                          (18, "rendering"),
                          (19, "capturing"),
                          (20, "object transfer"),
                          (21, "audio"),
                          (22, "telephony"),
                          (23, "information"))

    @inlineCallbacks
    def _init_(self, **kwargs):
        self.gw_is_master = self._Configs.get('core', 'is_master')
        self.gw_master_gateway = self._Configs.get('core', 'master_gateway')

        if HAS_BLUETOOTH is False:
            self.enabled = False
            self._Notifications.add({
                'title': 'Bluetooth driver missing',
                'message': 'The Bluetooth Presence module is missing critical python modules. Run the "sudo bash install.sh"'\
                    ' file inside the module folder.',
                'source': 'Zwave Module',
                'persist': False,
                'priority': 'high',
                'always_show': True,
                'always_show_allow_clear': True,
                # 'id': 'zwave_removed%s:%s' % (home_id, node_id),
            })

        else:
            self.enabled = True

        self.is_scanning = False
        self.discovery_scanner_reactor = None
        self.rssi_scanner_reactor = None
        self.locations = None  # Available locations
        try:
            self.this_location = self._ModuleVariables['location']['values'][0]
        except KeyError as e:
            self.this_location = None
        try:
            self.this_area = self._ModuleVariables['area']['values'][0]
        except KeyError as e:
            self.this_area = None
        self.scene = {}  # devices scene (addr:addr, rssi, time)
        self.device_history = {}

        # so we always scan in the same order.
        self.scannable_devices = OrderedDict()  # bt address = yombo_device_id

        self.discovered_devices = yield self._SQLDict.get(self, 'discovered_devices')

        self.presence_module = None

    def _start_(self, **kwargs):
        if self.enabled is False:
            logger.warn("bluetooth module not found, cannot start bluetooth presence.")
            return

        for device_id, device in self._ModuleDevices().items():
            if 'bluetooth_address' in device.device_variables:
                addr = device.device_variables['bluetooth_address']['values'][0]

                try:
                    bt_type = device.device_variables['bluetooth_type']['values'][0]
                    if bt_type not in ('bluetooth', 'ble'):
                        bt_type = 'bluetooth'
                except Exception as e:
                    bt_type = "bluetooth"

                self.scannable_devices[addr] = {
                    'bt_type': bt_type,
                    'yombo_device_id': device.device_id,
                    'BTRSSI': BluetoothRSSI(addr=addr)
                }

        self.discovery_scanner_reactor = reactor.callLater(10, self.start_discovery_scanner)
        self.discovery_rssi_reactor = reactor.callLater(5, self.start_discovery_scanner)

    def _stop_(self, **kwargs):
        logger.info("Waiting for bluetooth scanner to shut down.")
        if self.discovery_scanner_reactor is not None and self.discovery_scanner_reactor.active():
            self.discovery_scanner_reactor.canel()
        if self.discovery_rssi_reactor is not None and self.discovery_rssi_reactor.active():
            self.discovery_rssi_reactor.canel()

    def bluetoothpresence_presence_modules(self, **kwargs):
        """
        This is implemented by the presence module.

        :param kwargs:
        :return:
        """
        self.presence_module = kwargs['called_by']
        try:
            priority = self._ModuleVariables['port']['values'][0]
        except:
            priority = 100

        logger.debug("Registering Bluetooth Presence with Presence module")
        return {
            'fields': [
                {
                    'label':'Bluetooth ID',
                    'machine_label': 'bluetooth_id',
                 },
            ],
            'priority': priority,
            'module': self
        }

    def find_yombo_device(self, address):
        for device_id, device in self._ModuleDevices().items():
            if 'bluetooth_address' in device.device_variables:
                if device.device_variables['bluetooth_address']['values'][0] == address:
                    return device
        raise KeyError("No bluetooth presence device found for address: %s" % address)

    def add_device_discovered(self, addr, name, device_class):

        major_class = (device_class >> 8) & 0xf
        if major_class < 7:
            major_class = self.BT_MAJOR_CLASSES[major_class]
        else:
            major_class = print("Uncategorized")

        services = []
        for bitpos, classname in self.BT_SERVICE_CLASSES:
            if device_class & (1 << (bitpos-1)):
                services.append(classname)
        try:
            device = self.find_yombo_device(addr)
            if 'bluetooth_first_found' not in device.meta:
                device.meta['bluetooth_first_discovered'] = int(time())
            device.meta['bluetooth_last_discovered'] = int(time())
            device.meta['bluetooth_last_seen'] = int(time())
            device.meta['bluetooth_major_class'] = major_class
            device.meta['bluetooth_services'] = services
        except Exception as e:
            pass

        if addr not in self.discovered_devices:
            self.discovered_devices[addr] = {
                'name': name,
                'first_discovered': time(),
                'last_discovered': time(),
                'last_seen': time(),
            }

    @inlineCallbacks
    def start_discovery_scanner(self):
        """
        Search for bluetooth devices marked as findable.
        """
        # print("scan_bluetooth")
        if self.is_scanning is True:
            # print("try later...")
            self.schedule_discovery_scanner()
            return

        self.is_scanning = True
        # print("scan_bluetooth start")
        results = yield threads.deferToThread(self.do_discovery_scan)
        logger.debug("Bluetooth devices discovered = {result}, {asdf}", result=results,
                    asdf=type(results))
        self.is_scanning = False
        if len(results) > 0:
            for item in results:
                self.add_device_discovered(item[0], item[1], item[2])
        self.schedule_discovery_scanner()

    def schedule_discovery_scanner(self):
        if self.discovery_scanner_reactor is None:
            self.discovery_scanner_reactor = reactor.callLater(10, self.start_discovery_scanner)
        else:
            if self.discovery_scanner_reactor.active() is False:
                self.discovery_scanner_reactor = reactor.callLater(10, self.start_discovery_scanner)

    def do_discovery_scan(self):
        result = bluetooth.discover_devices(
            duration=8, lookup_names=True, flush_cache=True,
            lookup_class=True)
        logger.debug("Bluetooth devices discovered = {result}", result=result)
        return result

    def start_rssi_scanner(self):
        print("rssi_address: %s" % self.scannable_devices)
        yield threads.deferToThread(self.do_discovery_scan, self.scannable_devices)

    def schedule_rssi_scanner(self):
        if self.rssi_scanner_reactor is None:
            self.rssi_scanner_reactor = reactor.callLater(5, self.start_rssi_scanner)
        else:
            if self.rssi_scanner_reactor.active() is False:
                self.rssi_scanner_reactor = reactor.callLater(5, self.start_rssi_scanner)

    def rssi_results(self, scannable_device, rssi):
        print("got RSSI results: %s = %s" % (scannable_device, rssi))
        broadcast_presence_msg = self.presence_module.blank_presence()
        broadcast_presence_msg['location'] = self.this_location
        broadcast_presence_msg['area'] = self.this_area
        broadcast_presence_msg['presence_id'] = scannable_device['yombo_device_id']
        broadcast_presence_msg['rssi'] = rssi
        print("BC: %s" % broadcast_presence_msg)

    def do_get_rssi(self, devices):
        for device in devices:
            rssi = device['BTRSSI'].get_rssi()
            threads.blockingCallFromThread(reactor, self.rssi_results, device, rssi)
