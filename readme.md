Summary
=======

Adds bluetooth location tracking to the [Yombo Gateway](https://yombo.net/).

This module also requires the [Presence module](https://yombo.net/Modules/presence).
Additional details about this module at: https://yombo.net/Modules/bluetoothpresence

Learn about [Yombo Gateway](https://yombo.net/) or
[Get started today](https://yombo.net/Docs/Gateway/Quick_start)

Installation
============

Simply mark this module as being used by the gateway, and the gateway will
download and install this module automatically.

Start the gateway up fully after marking this module to be installed,
and then stop it.  Go into the module directory and execute the installer file:

sudo ./install.sh

Start the gateway up.

Requirements
============

This module relies on the [Presence module](https://yombo.net/docs/Modules/presence)
to perform many of the core functions.

The Yombo Gateway must have a Bluetooth receiver such as a USB dongle or
be running on a Raspberry Pi that has an integrated Bluetooth.


License
=======

The [Yombo](https://yombo.net/) team and other contributors
hopes that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See LICENSE file for full details.
